import { type MockRule } from '@ben_goodman/contentful-react-hooks'

const mockHeaderData = {
    data: {
        siteHeaderCollection: {
            items: [
                {
                    title: 'Mock Title',
                    subtitle: 'Mock Subtitle',
                    navigationLinksCollection: {
                        items: [
                            {
                                icon: {
                                    url: 'https://www.example.com/icon.png'
                                },
                                displayText: 'Mock Link',
                                url: 'https://www.example.com'
                            }
                        ]
                    }
                }
            ]
        }
    }
}

const mockBlogData = {
    'data': {
        'blogEntry': {
            'sys': {
                'publishedAt': '2023-09-04T01:14:16.346Z',
                'firstPublishedAt': '2023-09-03T04:15:43.980Z'
            },
            'canonicalTitle': 'test-article',
            'longTitle': 'Test Article',
            'subtitle': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet mattis vulputate enim.',
            'topics': [
                'Test',
                'CSS'
            ],
            'thumbnail': {
                'url': 'https://images.ctfassets.net/ogo0a7l5lbnd/4JzJPqkoz91Ym9GGoGlECl/d6e6e5e88bef6bbd74fa80ee8b8abfb9/Testing_Polarization.',
                'title': 'Test Image',
                'width': 640,
                'height': 361,
                'description': 'An image used for testing.'
            },
            body: {
                json: {
                    nodeType: 'document',
                    data: {},
                    content: [
                        {
                            nodeType: 'paragraph',
                            data: {},
                            content: [
                                {
                                    nodeType: 'text',
                                    value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Praesent tristique magna sit amet purus gravida quis blandit turpis. Sed felis eget velit aliquet sagittis. Vulputate dignissim suspendisse in est ante. Scelerisque purus semper eget duis at tellus. Aliquam purus sit amet luctus venenatis lectus magna. Sollicitudin aliquam ultrices sagittis orci a scelerisque. Rutrum tellus pellentesque eu tincidunt tortor. Aenean euismod elementum nisi quis. Magna fringilla urna porttitor rhoncus dolor purus non. Et netus et malesuada fames ac turpis egestas. Elit ut aliquam purus sit amet. Viverra mauris in aliquam sem fringilla. Rhoncus mattis rhoncus urna neque viverra justo nec ultrices dui. Ac feugiat sed lectus vestibulum mattis ullamcorper velit. Ipsum nunc aliquet bibendum enim facilisis. Diam ut venenatis tellus in metus vulputate.',
                                    marks: [],
                                    data: {}
                                }
                            ]
                        },
                    ]
                },
                links: {
                    entries: {
                        block: [],
                        inline: []
                    },
                    assets: {
                        block: []
                    }
                }
            }
        }
    }
}

export const mockRules: MockRule[] = [
    [
        'getDefaultHeader',
        () => new Promise((resolve) => {
            resolve(mockHeaderData)
        })
    ],
    [
        'getBlogArticle',
        () => new Promise(resolve => {
            resolve(mockBlogData)
        })
    ]
]