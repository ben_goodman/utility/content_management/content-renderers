import React, { type JSX } from 'react'
import {
    ContentfulAuthProvider,
    // ContentfulMockProvider,
    // ContentfulPreviewProvider
} from '@ben_goodman/contentful-react-hooks'

import { createGlobalStyle } from 'styled-components'
// import { mockRules } from './mocks'
import { DefaultHeader, BlogArticle } from '../src'
import { BlogPreview } from '../src/components/BlogPreview'

const TEST_ARTICLE_ID = '4Z8vPJYCfc27X15oiizt0Y'

const GlobalStyle = createGlobalStyle`
  :root {
    // --header_color-background: #282c34;
    // --header_color-border: #61dafb;
  }
  body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-style: normal;
    font-variant: normal;
  }
`

const titleClickHandler = () => {
    console.log('title clicked')
}

export const App = (): JSX.Element => {
    const [collapsed, setCollapsed] = React.useState<boolean>(false)
    const handeError = (error: Error) => {
        console.error(error)
    }
    return (
        <>
            <ContentfulAuthProvider
                spaceId={process.env.CONTENTFUL_SPACE_ID!}
                envId={process.env.CONTENTFUL_ENV_ID!}
                apiKey={process.env.CONTENTFUL_DELIVERY_TOKEN!}
                apiHost="graphql.contentful.com"
            >
                <button
                    onClick={() => {setCollapsed(s => !s)}}
                >
                    Click me
                </button>
                <GlobalStyle />
                <DefaultHeader
                    onTitleClick={titleClickHandler}
                    collapse={collapsed}
                />

                <div>
                    <BlogPreview
                        onError={handeError}
                        contentId={TEST_ARTICLE_ID} />

                    <hr />
                    <BlogArticle
                        onError={handeError}
                        contentId={TEST_ARTICLE_ID}/>
                </div>


            </ContentfulAuthProvider>
        </>
    )


}