# Content Components

A collection of React components for rendering content models defined by https://gitlab.com/ben_goodman/utility/content_management/content-models.

## Installation

Set the package's registry to the GitLab registry:

```bash
echo "@ben_goodman:registry=https://gitlab.com/api/v4/packages/npm/" >> .npmrc
```

Install the package with `npm i "@ben_goodman/content-components`.

