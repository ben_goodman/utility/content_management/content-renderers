import React from 'react'
import { documentToReactComponents, type Options as RenderOptions } from '@contentful/rich-text-react-renderer'
import { BLOCKS, INLINES, type Document } from '@contentful/rich-text-types'

export interface LinkNode extends Record<string, string|object> {
    sys: {
        id: string
    },
    __typename: string,
}

export interface RichTextLinks {
    assets: {
        block: Array<LinkNode>,
    },
    entries: {
        block: Array<LinkNode>,
        inline: Array<LinkNode>,
    },
}

export interface RichTextField {
    json: Document,
    links: RichTextLinks
}

export type RenderFunction = (_) => JSX.Element

export type TypeHandlers = { [typename:string]: RenderFunction }

export type RenderOpts = {
    'embedded-entry-inline': TypeHandlers,
    'embedded-entry-block': TypeHandlers,
}

const default_render_opts: RenderOpts = {
    'embedded-entry-inline': {},
    'embedded-entry-block': {},
}

const generateRenderOpts = (
    links: RichTextLinks,
    renderOpts: RenderOpts
): RenderOptions => {

    // create an asset map
    const assetMap = new Map()
    // loop through the assets and add them to the map
    for (const asset of links.assets.block) {
        assetMap.set(asset.sys.id, asset)
    }

    // create an entry map
    const entryMap = new Map()
    // loop through the block linked entries and add them to the map
    for (const entry of links.entries.block) {
        entryMap.set(entry.sys.id, entry)
    }

    // loop through the inline linked entries and add them to the map
    for (const entry of links.entries.inline) {
        entryMap.set(entry.sys.id, entry)
    }

    return {
        // other options...
        renderNode: {
            [INLINES.EMBEDDED_ENTRY]: (node) => {
                // find the entry in the entryMap by ID
                const entry = entryMap.get(node.data.target.sys.id)
                const handlerTuples = Object.entries(
                    renderOpts[INLINES.EMBEDDED_ENTRY]
                )
                for (const [typename, renderFunction] of handlerTuples) {
                    if (entry.__typename === typename) {
                        return renderFunction(entry)
                    }
                }
            },
            [BLOCKS.EMBEDDED_ENTRY]: (node) => {
                // find the entry in the entryMap by ID
                const entry = entryMap.get(node.data.target.sys.id)
                const handlerTuples = Object.entries(
                    renderOpts[BLOCKS.EMBEDDED_ENTRY]
                )
                for (const [typename, renderFunction] of handlerTuples) {
                    if (entry.__typename === typename) {
                        return renderFunction(entry)
                    }
                }
            },
            [BLOCKS.EMBEDDED_ASSET]: (node) => {
                // find the asset in the assetMap by ID
                const asset = assetMap.get(node.data.target.sys.id)
                // render the asset accordingly
                return (
                    <img src={asset.url} alt={asset.description} />
                )
            },
        },
    }
}

export const renderRichTextField = (
    field: RichTextField,
    renderOpts: RenderOpts = default_render_opts
) => {
    return documentToReactComponents(
        field.json,
        generateRenderOpts(field.links, renderOpts)
    )
}