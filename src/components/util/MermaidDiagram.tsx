import React, { type JSX, useEffect } from 'react'
import styled from 'styled-components'
import { type IMermaidDiagramFields } from '@ben_goodman/content-model-types'

declare global {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    interface Window { mermaid: any; }
}

window.mermaid = window.mermaid || {}

const DEFAULT_CONFIG = {
    startOnLoad: true,
    theme: 'default',
    logLevel: 'fatal',
    securityLevel: 'loose',
    arrowMarkerAbsolute: false,
}

const Border = styled.div`
  margin-bottom: 2em;
  border: 1px solid black;
  display: block;
  margin-block-start: 1em;
  margin-block-end: 1em;
  margin-inline-start: 0px;
  margin-inline-end: 0px;

`

const Diagram = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`

const Caption = styled.p`
  font-size: 0.9rem;
  padding-left: 1.5em;
`

const loadScript = (src: string) => {
    return new Promise<void>((resolve, reject) => {
        if (typeof window !== 'undefined') {
            const script = window.document.createElement('script')
            script.src = src
            script.addEventListener('load', function () {
                resolve()
            })
            script.addEventListener('error', function (e) {
                reject(e)
            })
            window.document.body.appendChild(script)
        }
    })
}

export const MermaidDiagram = ({
    title = undefined,
    caption = undefined,
    diagram
}: IMermaidDiagramFields): JSX.Element => {
    useEffect(() => {
        const renderMermaid = async () => {
            if (!window.mermaid.mermaidAPI) {
                await loadScript('https://cdnjs.cloudflare.com/ajax/libs/mermaid/9.3.0/mermaid.min.js')
            }
            window.mermaid.mermaidAPI.initialize(DEFAULT_CONFIG)
            window.mermaid.contentLoaded()
        }
        void renderMermaid()
    }, [])

    return (
        <Border>
            <Diagram>
                <pre className="mermaid" title={title}>{diagram || 'Loading'}</pre>
            </Diagram>
            {caption && <Caption>{caption}</Caption>}
        </Border>
    )
}