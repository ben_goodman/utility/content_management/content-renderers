/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { type JSX } from 'react'
import styled from 'styled-components'
import { IFancyLinkFields } from '@ben_goodman/content-model-types'

export interface FancyLinkProps extends Omit<IFancyLinkFields, 'icon'> {
    key?: React.Key;
    icon: string
}

const StyledFancyLink = styled.a`
    display: inline-block;
    margin: 0 0.5rem;
    img {
        width: 2rem;
        height: 2rem;
    }
`

export const FancyLink = ({
    displayText,
    url,
    icon
}: FancyLinkProps): JSX.Element => {
    return (
        <StyledFancyLink href={url}>
            <img src={icon} alt={displayText} />
        </StyledFancyLink>
    )
}