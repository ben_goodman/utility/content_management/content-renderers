import React, { type JSX, useEffect } from 'react'
import Prism from 'prismjs'

import { type ICodeSnippetFields } from '@ben_goodman/content-model-types'

export interface CodeSnippetProps extends ICodeSnippetFields {
  inline?: boolean;
}

const CodeInline = ({style, code, description}: CodeSnippetProps) => {
    return (
        <code className={`language-${style}`} aria-description={description}>{code}</code>
    )
}

const CodeBlock = (props: CodeSnippetProps) => {
    return (
        <pre>
            <CodeInline {...props} />
        </pre>
    )
}


export const CodeSnippet = ({
    title = undefined,
    description,
    style,
    code,
    inline = false
}: CodeSnippetProps): JSX.Element => {

    const codeProps = {style, code, description}

    useEffect(() => {
        if (style !== 'text') {
            Prism.highlightAll()
        }
    }, [])

    return (
        <>
            {title
                ? <span>{title}</span>
                : <></>
            }
            {
                inline
                    ? <CodeInline {...codeProps}/>
                    : <CodeBlock {...codeProps}/>
            }
        </>
    )
}