import styled from 'styled-components'
import '../../../assets/fonts/FlowBlock/index.css'

export const SkeletonWrapperStyle = styled.div<{ $isLoading?: boolean }>`
    transition-property: opacity, color;
    transition: color 0.5s ease-in-out;

    ${props => props.$isLoading &&  `
        font-family: 'FlowBlock', sans-serif;
        color: #d0cdcd;
    `}
`