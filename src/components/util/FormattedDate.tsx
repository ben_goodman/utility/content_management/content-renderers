import React from 'react'

const addOrdinal = (num: number) => {
    switch(num){
    case 1:
        return num + 'st'
        break
    case 2:
        return num + 'nd'
        break
    case 3:
        return num + 'rd'
        break
    default:
        return num + 'th'
    }
}

export const FormattedDate = ({time}: {time: string}) => {
    const date = new Date(time)
    const datesString = `${addOrdinal(date.getDate())} ${date.toLocaleString('default', {month: 'long'})} ${date.getFullYear()}`
    return (
        <p className='formattedDate'>
            {datesString}
        </p>
    )
}