import React, { PropsWithChildren } from 'react'
import styled from 'styled-components'

interface PreviewCardProps extends PropsWithChildren {
    key?: React.Key
    onClick?: (event) => void
}

const PreviewCardStyle = styled.div<{ $isLoading?: boolean }>`
    flex-direction: column;
    justify-content: center;
    align-items: center;
    max-width: 800px;
    border: 1px solid black;
    border-radius: 5px;
    padding: 10px;
    margin: 10px;
    transition: all 0.2s ease-in-out;
    cursor: pointer;

    &:hover {
        background-color: #f0f0f0;
    }

    ${ props => props.$isLoading && `
        font-family: "FlowBlock";
    `}
`


export const PreviewWrapper = ({
    children,
    onClick = () => {}
}: PreviewCardProps) => {
    return (
        <PreviewCardStyle onClick={onClick}>
            {children}
        </PreviewCardStyle>
    )
}