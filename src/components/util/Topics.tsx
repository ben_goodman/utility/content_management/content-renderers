import React from 'react'

export const Topics = ({topics}: {topics: Array<string>}) => {
    return (
        <div>
            {topics?.map((topic, i) => <span key={`${i}`}>{topic.toLowerCase()}</span>)}
        </div>
    )
}