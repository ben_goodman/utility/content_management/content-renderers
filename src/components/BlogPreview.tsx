import React from 'react'
import { BlogArticle, BlogArticleProps } from './BlogArticle'
import { PreviewWrapper } from './util/PreviewWrapper'

export interface BlogPreviewProps extends BlogArticleProps {
    key?: React.Key
    onPreviewClick?: () => void
}

export const BlogPreview = ({
    contentId,
    onPreviewClick = () => {}
}: BlogPreviewProps) => {
    return (
        <PreviewWrapper onClick={onPreviewClick}>
            <BlogArticle
                contentId={contentId}
                showContent={false} />
        </PreviewWrapper>
    )
}