import React from 'react'
import styled from 'styled-components'
import { useBlogArticleFields } from '../hooks'
import { CodeSnippet } from './util/CodeSnippet'
import { FormattedDate } from './util/FormattedDate'
import { SkeletonWrapperStyle } from './util/SkeletonWrapperStyle'
import { type RichTextField, renderRichTextField } from '../util/renderRichTextField'
import { type IMermaidDiagramFields, type ICodeSnippetFields } from '@ben_goodman/content-model-types'
import { MermaidDiagram } from './util/MermaidDiagram'
import { BlogArticleFields } from '../hooks/useBlogArticleFields'


const renderBody = (rTextField: RichTextField) => {
    const handlers = {
        'embedded-entry-block': {
            CodeSnippet: (props: ICodeSnippetFields) => {
                return <CodeSnippet {...props} />
            },
            MermaidDiagram: (props: IMermaidDiagramFields) => {
                return <MermaidDiagram {...props} />
            },
        },
        'embedded-entry-inline': {
            CodeSnippet: (props: ICodeSnippetFields) => {
                return <CodeSnippet {...props} inline={true} />
            },
        }
    }

    return renderRichTextField(rTextField, handlers)
}

export interface BlogArticleProps {
    preview?: boolean;
    contentId?: string;
    showContent?: boolean;
    onError?: (error: Error) => void;
    onTitleClick?: () => void;
}

const BlogStyleWrapper = styled.div`
    line-height: 1.8;

    > h1 {
        line-height: 1.1;
        margin-top: 0px;
    }

    blockquote {
        border-left: 0.2em solid black;
        padding-left: 1.5em;
        background: aliceblue;
    }
`

const BannerImage = styled.div<{ $src: string }>`
    background-repeat: no-repeat;
    min-width: 17em;
    min-height: 17em;
    background-size: cover;
    background-position: center;
    background-image: url(${props => props.$src});
`

const BlogHeaderStyle = styled.header<{ $inline: boolean }>`
    ${props => props.$inline ? 'display: flex;' : 'display: block;'}

    flex-direction: row;
    justify-content: flex-start;
    align-items: flex-start;


    p.formattedDate {
        font-size: 1.2rem;
        color: #5f5f5f;
        margin-top: 0px;
        margin-bottom: 1.5rem;
    }

    p.blogTitle {
        font-size: 2rem;
        margin-bottom: 1rem;
        margin-top: 0px;
        font-weight: bold;
    }

    p.blogSubtitle {
        margin-top: 0px;
        font-size: 1.5rem;
        line-height: 1.5;
    }
`

interface BlogPreviewProps extends Omit<BlogArticleFields, 'body'> {
    inline: boolean;
    onTitleClick?: () => void;
}

const BlogHeader = ({
    longTitle, meta, thumbnail, subtitle, inline, onTitleClick
}: BlogPreviewProps) => {
    return (
        <BlogHeaderStyle $inline={inline}>
            <div>
                <p className="blogTitle" onClick={onTitleClick}>{longTitle}</p>
                <FormattedDate time={meta.publishedAt} />
                <p className='blogSubtitle'>{subtitle}</p>
            </div>
            <BannerImage $src={thumbnail.url} />
        </BlogHeaderStyle>
    )
}

export const BlogArticle = ({
    contentId = undefined,
    showContent = true,
    onError = () => { },
    onTitleClick = () => { }
}: BlogArticleProps) => {
    const {
        entry,
        loading,
        error
    } = useBlogArticleFields({ contentId, showContent })

    if (error) {
        onError(error)
    }

    return (
        <SkeletonWrapperStyle $isLoading={loading || error}>
            <BlogHeader {...entry} inline={!showContent}
                onTitleClick={onTitleClick} />
            <BlogStyleWrapper>
                {showContent ? renderBody(entry.body) : null}
            </BlogStyleWrapper>
        </SkeletonWrapperStyle>
    )
}
