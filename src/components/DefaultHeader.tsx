import React from 'react'
import { useDefaultHeaderFields } from '../hooks/useDefaultHeaderFields'
import { FancyLink } from './util/FancyLink'
import { SkeletonWrapperStyle } from './util/SkeletonWrapperStyle'
import styled from 'styled-components'

export interface HeaderProps {
    collapse?: boolean;
    onTitleClick?: () => void;
}

const HeaderStyleWrapper = styled.header<{ $collapse: boolean }>`
    display: flex;
    align-items: baseline;
    flex-direction: column;

    background-color: var(--header_color-background);
    border-bottom: 0.1em solid var(--header_color-border);

    .headerItem {
        margin: 0.5rem 0 0.5rem 0;
    }

    .collapsible {
        overflow: hidden;
        height: auto;
        max-height: ${ props => props.$collapse ? '0' : '50px'};
        margin: ${ props => props.$collapse ? '0' : '0.5rem 0 0.5rem 0'};
        transition: margin, max-height 0.3s ease-out;
    }

    h1.headerTitle {
        font-size: 3rem;
        font-weight: bold;
    }

    h2.headerSubtitle {
        font-size: 2rem;
    }

    .navLinks {
        display: flex;
        flex-direction: row;
        align-items: inherit;
        justify-content: inherit;
    }
`

export const DefaultHeader = ({
    collapse = false,
    onTitleClick = () => {}
}: HeaderProps): JSX.Element => {
    const {entry, loading, error} = useDefaultHeaderFields()

    return (
        <SkeletonWrapperStyle $isLoading={loading || error}>
            <HeaderStyleWrapper $collapse={collapse}>
                <h1
                    className='headerTitle headerItem collapsible'
                    onClick={onTitleClick}
                    style={{cursor: onTitleClick ? 'pointer' : 'default'}}
                >
                    {entry.title}
                </h1>

                <h2 className='headerSubtitle headerItem collapsible'>{entry.subtitle}</h2>

                <div className="navLinks headerItem">
                    {entry.navigationLinks.map((link, index) => (
                        <FancyLink key={index} {...link} />
                    ))}
                </div>

            </HeaderStyleWrapper>
        </SkeletonWrapperStyle>
    )
}