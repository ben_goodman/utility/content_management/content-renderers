import { BLOCKS } from '@contentful/rich-text-types'
import { useContentfulQuery } from '@ben_goodman/contentful-react-hooks'
import { type IBlogEntryFields } from '@ben_goodman/content-model-types'
import { getBlogArticleQuery } from './queries/getBlogArticle'
import { type RichTextField } from '../util/renderRichTextField'

export interface BlogArticleFields extends Omit<IBlogEntryFields, 'body' | 'thumbnail'> {
    body?: RichTextField,
    meta: {
        publishedAt: string,
        firstPublishedAt: string,
    },
    thumbnail: {
        url: string,
        title: string,
        description: string,
    }
}

const reducer = async (resp): Promise<BlogArticleFields> => {
    return new Promise((resolve, reject) => {
        if (resp?.errors) {
            reject(new Error(`[500] ${JSON.stringify(resp.errors)}`))
        }

        try {
            const data = resp.data.blogEntry

            if (data === null) {
                reject(new Error('[404] No matching blog article was found.'))
            }

            const fields = {
                canonicalTitle: data.canonicalTitle,
                longTitle: data.longTitle,
                topics: data.topics,
                subtitle: data.subtitle,
                thumbnail: {
                    url: data.thumbnail.url,
                    title: data.thumbnail.title,
                    description: data.thumbnail.description,
                },
                body: data?.body,
                meta: {
                    publishedAt: data.sys.publishedAt,
                    firstPublishedAt: data.sys.firstPublishedAt,
                }
            }

            resolve(fields)
        } catch (_) {
            reject(
                new Error(`Error parsing blog article data from response: ${JSON.stringify(resp)}`)
            )
        }
    })
}

// used to initialize the blog article fields with skeleton data
const init: BlogArticleFields = {
    topics: [],
    canonicalTitle: 'lorem-ipsum',
    longTitle: 'Lorem Ipsum Dolor ',
    subtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    thumbnail: {
        url: new URL('../../assets/images/image-placeholder.png', import.meta.url).toString(),
        title: 'placeholder',
        description: 'A placeholder image.'
    },
    meta: {
        publishedAt: new Date().toISOString(),
        firstPublishedAt: new Date().toISOString(),
    },
    body: {
        json: {
            nodeType: BLOCKS.DOCUMENT,
            data: {},
            content: [
                {
                    nodeType: BLOCKS.PARAGRAPH,
                    data: {},
                    content: [
                        {
                            nodeType: 'text',
                            value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Praesent tristique magna sit amet purus gravida quis blandit turpis. Sed felis eget velit aliquet sagittis. Vulputate dignissim suspendisse in est ante. Scelerisque purus semper eget duis at tellus. Aliquam purus sit amet luctus venenatis lectus magna. Sollicitudin aliquam ultrices sagittis orci a scelerisque. Rutrum tellus pellentesque eu tincidunt tortor. Aenean euismod elementum nisi quis. Magna fringilla urna porttitor rhoncus dolor purus non. Et netus et malesuada fames ac turpis egestas. Elit ut aliquam purus sit amet. Viverra mauris in aliquam sem fringilla. Rhoncus mattis rhoncus urna neque viverra justo nec ultrices dui. Ac feugiat sed lectus vestibulum mattis ullamcorper velit. Ipsum nunc aliquet bibendum enim facilisis. Diam ut venenatis tellus in metus vulputate.',
                            marks: [],
                            data: {}
                        }
                    ]
                },
            ]
        },
        links: {
            entries: {
                block: [],
                inline: []
            },
            assets: {
                block: []
            }
        }
    }

}

export type UseBlogArticleFields = { contentId: string, showContent?: boolean };

export const useBlogArticleFields = ({
    contentId,
    showContent = true
}: UseBlogArticleFields) => {
    return useContentfulQuery<BlogArticleFields>(
        getBlogArticleQuery,
        {
            variables: {
                showContent,
                contentId
            },
            reducer,
            init
        }
    )
}
