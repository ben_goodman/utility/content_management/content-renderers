export const defaultHeaderQuery =  `
query getDefaultHeader ($preview: Boolean) {
	siteHeaderCollection(limit:1, where:{default: true}, preview: $preview) {
    items {
      __typename
      sys {
        id
      }
      title
      subtitle
      navigationLinksCollection {
        items {
          icon {
            url
          }
          displayText
          url
        }
      }
    }
  }
}`