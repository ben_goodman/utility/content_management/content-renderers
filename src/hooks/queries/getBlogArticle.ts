const blogContentFragment = `fragment content on BlogEntryBody {
  json
  links {
    entries {
      block {
        sys {
          id
        }
        __typename
        ... on CodeSnippet {
          __typename
          sys {
            id
          }
          title
          style
          code
          description
        }
        ... on MermaidDiagram {
          __typename
          sys {
            id
          }
          diagram
          title
          caption
          description
        }
      }
      inline {
        sys {
          id
        }
        __typename
        ... on CodeSnippet {
          __typename
          sys {
            id
          }
          title
          style
          code
          description
        }
      }
    }
    assets {
      block {
        sys {
          id
        }
        url
        title
        width
        height
        description
      }
    }
  }
}
`

export const getBlogArticleQuery = `
${blogContentFragment}
query getBlogArticle ($contentId: String!, $showContent: Boolean!, $preview: Boolean) {
  blogEntry(id: $contentId, preview: $preview) {
    __typename
    canonicalTitle
    longTitle
    subtitle
    topics
    sys {
      id
      publishedAt
      firstPublishedAt
    }
    thumbnail {
      url
      title
      width
      height
      description
    }
    body @include(if: $showContent) {
      ...content
    }
  }
}
`