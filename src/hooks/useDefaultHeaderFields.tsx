import { type ISiteHeaderFields, type IFancyLinkFields } from '@ben_goodman/content-model-types'
import { useContentfulQuery } from '@ben_goodman/contentful-react-hooks'
import { defaultHeaderQuery } from './queries/defaultHeaderQuery'

export interface HeaderFields extends Omit<ISiteHeaderFields, 'navigationLinks'> {
    navigationLinks: Array<{ icon: string } & Omit<IFancyLinkFields, 'icon'>>
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const reducer = async (resp: any): Promise<HeaderFields> => {
    /**
    {
      "data": {
        "siteHeaderCollection": {
          "items": [
            {
              "title": "Main Header",
              "subtitle": "A testing subtitle.",
              "navigationLinksCollection": {
                "items": [
                  {
                    "displayText": "Email",
                    "url": "mailto:hello@ben.website",
                    "icon": {
                      "url": "https://images.ctfassets.net/ogo0a7l5lbnd/69dOpm59UahyawKL8yBA0t/b25cd44b88365061393dff47b27d38dd/email_icon.png"
                    }
                  },
                  {
                    "displayText": "GitLab",
                    "url": "https://gitlab.com/ben_goodman",
                    "icon": {
                      "url": "https://images.ctfassets.net/ogo0a7l5lbnd/28c97WkrgCqrLzxaBN7BPd/9dc2d818d3807420acbfcab94418dcb3/gitlab_icon.png"
                    }
                  },
                  {
                    "displayText": "LinkedIn",
                    "url": "https://www.linkedin.com/in/benjamin-goodman/",
                    "icon": {
                      "url": "https://images.ctfassets.net/ogo0a7l5lbnd/7k4VDg4qWN1g3L82ppqjKr/c228b653c82bc40b06f2de952583c321/linkedin_icon.png"
                    }
                  }
                ]
              }
            }
          ]
        }
      }
    }
     */
    return new Promise((resolve, reject) => {
        if (resp.errors) {
            reject(new Error(`[500] ${JSON.stringify(resp.errors)}`))
        }

        try {
            if (resp.data.siteHeaderCollection.items.length === 0) {
                reject(new Error('[404] No matching header was found.'))
            }

            const data = resp.data.siteHeaderCollection.items[0]

            const fields = {
                title: data.title,
                subtitle: data.subtitle,
                navigationLinks: data.navigationLinksCollection.items.map(
                    (item) => ({
                        displayText: item.displayText,
                        url: item.url,
                        icon: item.icon.url,
                    }))
            }

            resolve(fields)
        } catch (_) {
            reject(
                new Error(`Error parsing header data from response: ${JSON.stringify(resp)}`)
            )
        }
    })
}


export const useDefaultHeaderFields = (init: HeaderFields = {
    title: 'Lorem ipsum',
    subtitle: 'Sit amet mattis vulputate enim',
    navigationLinks: []
}) => {
    return useContentfulQuery<HeaderFields>(
        defaultHeaderQuery, { reducer, init }
    )
}
